package fr.ensicaen.genielogiciel.mvp.view;

import fr.ensicaen.genielogiciel.mvp.controller.ControleurScores;
import fr.ensicaen.genielogiciel.mvp.controller.Ecrans;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextArea;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class VueScore implements Initializable {
    private ControleurScores controleur;
    @FXML
    public TextArea TArea ;

    public void setControleur(ControleurScores _controleur) {
        controleur = _controleur;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }
    public void onShowScores() {
        TArea.setText(controleur.loadScoreFile());
    }
}

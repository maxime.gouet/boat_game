package fr.ensicaen.genielogiciel.mvp.view;

import fr.ensicaen.genielogiciel.mvp.controller.ControleurMenuPrincipal;
import fr.ensicaen.genielogiciel.mvp.controller.Ecrans;
import javafx.fxml.Initializable;
import javafx.scene.input.MouseEvent;

import java.net.URL;
import java.util.ResourceBundle;

public class VueMenuPrincipal implements Initializable {
    private ControleurMenuPrincipal controleur;
    private Ecrans EcranSuivant;

    public void setControleur(ControleurMenuPrincipal _controleur) {
        controleur = _controleur;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    public void onJouerClick(MouseEvent mouseEvent) {
        EcranSuivant = Ecrans.JEU;
        controleur.onLblClick();
    }

    public void onScoreClick(MouseEvent mouseEvent) {
        EcranSuivant = Ecrans.SCORE;
        controleur.onLblClick();
    }

    public Ecrans getEcranSuivant() {
        return EcranSuivant;
    }
}

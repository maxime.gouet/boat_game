package fr.ensicaen.genielogiciel.mvp.view;

import fr.ensicaen.genielogiciel.mvp.RegateApplication;
import fr.ensicaen.genielogiciel.mvp.controller.ControleurJeu;
import fr.ensicaen.genielogiciel.mvp.model.jeu.*;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.control.TextInputDialog;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;
import javafx.scene.shape.Rectangle;

import java.net.URL;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;

public class VueJeu implements Initializable {
  private ControleurJeu controleur;
  private Rectangle representation_voilier;

  private final double WIDTH_BATEAU = 10.0;
  private final double HEIGHT_BATEAU = 5.0;

  @FXML
  Pane container;
  @FXML
  Label lbl_vitesse_voilier;
  @FXML
  Label lbl_direction_vent;
  @FXML
  Label lbl_vitesse_vent;
  @FXML
  Label lbl_angle;
  @FXML
  Label lbl_timer;
  @FXML
  Pane statusBar;

  public void setControleur(ControleurJeu _controleur) {
    controleur = _controleur;
  }

  @Override
  public void initialize(URL location, ResourceBundle resources) {
    RegateApplication.getScene().addEventFilter(KeyEvent.KEY_PRESSED, keyEvent -> {
      switch (keyEvent.getCode()) {
        case LEFT -> {
          controleur.moveOrientation(-0.1);
        }
        case RIGHT -> {
          controleur.moveOrientation(0.1);
        }
      }
    });

  }

  public void initDashboard() {
    lbl_vitesse_voilier.textProperty().bind(controleur.getVitesseProperty());
    lbl_direction_vent.textProperty().bind(controleur.getDirectionVentProperty());
    lbl_vitesse_vent.textProperty().bind(controleur.getVitesseVentProperty());
    lbl_angle.textProperty().bind(controleur.getOrientationProperty());
    lbl_timer.textProperty().bind(controleur.getTimerProperty());
  }

  public void setBouees(ArrayList<Bouee> bouees) {
    bouees.forEach((bouee -> {
      Point pos = bouee.getPosition();
      Circle representation_bouee = new Circle(pos.getX(), pos.getY(), bouee.getRayon());
      representation_bouee.setFill(Color.RED);
      container.getChildren().add(representation_bouee);
    }));
  }

  public void setBordure(Bordure bordure) {
    ArrayList<Point> arrayList = bordure.getPolygone().getListePoints();
    double[] points = new double[arrayList.size() * 2];
    int i = 0;
    for (Point point : arrayList) {
      points[i] = point.getX();
      points[i + 1] = point.getY();
      i += 2;
    }
    Polygon representation_bordure = new Polygon(points);
    representation_bordure.setFill(Color.LIGHTBLUE);
    representation_bordure.setStroke(Color.RED);
    container.getChildren().add(representation_bordure);
  }

  public void setVoilier(Voilier voilier) {
    updateRepresentationVoilier(voilier.getPosition(), voilier.getRotation());
  }

  public void updateRepresentationVoilier(Vector2D posVoilier, int orientation) {
    if (representation_voilier != null) {
      container.getChildren().remove(representation_voilier);
    }

    representation_voilier = new Rectangle(posVoilier.x-WIDTH_BATEAU/2, posVoilier.y-HEIGHT_BATEAU/2, WIDTH_BATEAU, HEIGHT_BATEAU);
    representation_voilier.setRotate(orientation-90);
    representation_voilier.setFill(Color.PURPLE);

    container.getChildren().add(representation_voilier);
  }

  public void setLigneArrivee(LigneArrive ligne) {
    ArrayList<Point> arrayList = ligne.getPolygone().getListePoints();
    double[] points = new double[arrayList.size() * 2];
    int i = 0;
    for (Point point : arrayList) {
      points[i] = point.getX();
      points[i + 1] = point.getY();
      i += 2;
    }
    Polygon representation_ligne = new Polygon(points);
    representation_ligne.setFill(Color.FORESTGREEN);
    representation_ligne.setStroke(Color.FORESTGREEN);
    container.getChildren().add(representation_ligne);
  }

  public void afficherDefaite() {
    Alert alert = new Alert(Alert.AlertType.ERROR);
    alert.setTitle("Défaite");
    alert.setHeaderText("DEFAITE");
    alert.setContentText("Vous venez de heurter un obstacle...");

    Platform.runLater(alert::showAndWait);
  }

  public String afficherVictoireAndGetPseudo() {
    TextInputDialog dialog = new TextInputDialog();

    dialog.setTitle("VICTOIRE");
    dialog.setHeaderText("Bravo vous avez gagné !");
    dialog.setContentText("Pseudo : ");

    Optional<String> result = dialog.showAndWait();
    String toReturn = null;

    if (result.isPresent()) {
      toReturn = result.get();
    }

    return toReturn;
  }
}

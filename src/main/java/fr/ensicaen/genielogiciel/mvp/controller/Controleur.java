package fr.ensicaen.genielogiciel.mvp.controller;

import fr.ensicaen.genielogiciel.mvp.RegateApplication;
import javafx.scene.Parent;

import java.io.IOException;
import java.util.ArrayList;

public abstract class Controleur {
    protected ArrayList<Observateur> observateurs;

    protected Controleur() {
        observateurs = new ArrayList<>();
    }

    public void ajouterObservateur(Observateur app) {
        if (!observateurs.contains(app))
            observateurs.add(app);
    }

    public void retirerObservateur(RegateApplication app) {
        observateurs.remove(app);
    }

    public abstract void notifier();
    public abstract void initialisation() throws IOException;
    public abstract Parent getRoot();
}

package fr.ensicaen.genielogiciel.mvp.controller;

import fr.ensicaen.genielogiciel.mvp.model.jeu.PolaireDeVitesse;

import java.io.IOException;

public interface IChargerPolaireDeVitesse {
    public PolaireDeVitesse charger() throws IOException;
}

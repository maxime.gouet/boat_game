package fr.ensicaen.genielogiciel.mvp.controller;

import fr.ensicaen.genielogiciel.mvp.RegateApplication;
import fr.ensicaen.genielogiciel.mvp.model.jeu.GameManager;
import fr.ensicaen.genielogiciel.mvp.model.jeu.ObservateurVoilier;
import fr.ensicaen.genielogiciel.mvp.model.score.Ranking;
import fr.ensicaen.genielogiciel.mvp.view.VueJeu;
import javafx.animation.AnimationTimer;
import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class ControleurJeu extends Controleur implements ObservateurVoilier {
    private VueJeu vue;
    private Parent root;
    private GameManager model;
    private Ecrans ecranSuivant;
    private long timer_start;

    private SimpleStringProperty vitesseProperty;
    private SimpleStringProperty orientationProperty;
    private SimpleStringProperty vitesseVentProperty;
    private SimpleStringProperty directionVentProperty;
    private SimpleStringProperty timerProperty;

    public ControleurJeu() {
        super();
    }

    @Override
    public void notifier() {
        observateurs.forEach((observateur -> {
            observateur.actualiser(ecranSuivant);
        }));
    }

    public void jouer() {
        AnimationTimer timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                long timer_current = System.currentTimeMillis();
                long timer = timer_current - timer_start;

                timerProperty.set(String.format("%02d:%02d",
                        TimeUnit.MILLISECONDS.toMinutes(timer),
                        TimeUnit.MILLISECONDS.toSeconds(timer) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(timer))
                ));

                model.getVoilier().deplacer(model.getCarte().getVent());
                vue.updateRepresentationVoilier(model.getVoilier().getPosition(), model.getVoilier().getRotation());

                if (model.verifierCollisions()) {
                    this.stop();
                    vue.afficherDefaite();
                    ecranSuivant = Ecrans.MENU_PRINCIPAL;
                    notifier();
                } else if (model.verifierLigneArrivee()) {
                    Platform.runLater(victory(timer));
                    this.stop();
                    ecranSuivant = Ecrans.SCORE;
                    notifier();
                }
            }
        };

        timer.start();
        timer_start = System.currentTimeMillis();
    }

    private Runnable victory(long timer) {
        return () -> {
            String pseudo = vue.afficherVictoireAndGetPseudo();
            Ranking rank = new Ranking();
            rank.load();
            rank.add(pseudo, timer);
            rank.save();
        };
    }

    public void moveOrientation(double step) {
        model.getVoilier().updateOrientation(step);
    }

    @Override
    public void initialisation() throws IOException {
        FXMLLoader loader = new FXMLLoader(RegateApplication.class.getResource("jeu.fxml"), RegateApplication.getMessageBundle());
        root = loader.load();
        vue = loader.getController();
        vue.setControleur(this);

        model = new GameManager();
        model.initialisation();

        model.getVoilier().addObservateur(this);

        vue.setBordure(model.getCarte().getBordure());
        vue.setBouees(model.getCarte().getBouees());
        vue.setLigneArrivee(model.getCarte().getLigneArrive());
        vue.setVoilier(model.getVoilier());

        orientationProperty = new SimpleStringProperty(model.getVoilier().getRotation()+"");
        vitesseProperty = new SimpleStringProperty(model.getVoilier().getVitesse()+"");
        directionVentProperty = new SimpleStringProperty(model.getCarte().getVent().getDirection()+"");
        vitesseVentProperty = new SimpleStringProperty(model.getCarte().getVent().getVitesse()+"");
        timerProperty = new SimpleStringProperty("00:00");

        vue.initDashboard();

        jouer();
    }

    @Override
    public Parent getRoot() {
        return root;
    }

    @Override
    public void vitesseUpdated(double vitesse) {
        vitesseProperty.set(""+vitesse);
    }

    @Override
    public void directionParRapportAuVentUpdated(int direction) {
        orientationProperty.set(direction+"°");
    }

    public SimpleStringProperty getVitesseProperty() {
        return vitesseProperty;
    }

    public SimpleStringProperty getOrientationProperty() {
        return orientationProperty;
    }

    public SimpleStringProperty getVitesseVentProperty() {
        return vitesseVentProperty;
    }

    public SimpleStringProperty getDirectionVentProperty() {
        return directionVentProperty;
    }

    public SimpleStringProperty getTimerProperty() {
        return timerProperty;
    }
}

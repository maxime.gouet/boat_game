package fr.ensicaen.genielogiciel.mvp.controller;

public interface Observateur {
    public void actualiser(Ecrans nouveauControleur);
}

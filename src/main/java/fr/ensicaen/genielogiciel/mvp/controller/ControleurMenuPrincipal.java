package fr.ensicaen.genielogiciel.mvp.controller;

import fr.ensicaen.genielogiciel.mvp.RegateApplication;
import fr.ensicaen.genielogiciel.mvp.view.VueMenuPrincipal;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.io.IOException;

public class ControleurMenuPrincipal extends Controleur {
    private VueMenuPrincipal vue;
    private Parent root;

    public ControleurMenuPrincipal() {
        super();
    }

    @Override
    public void notifier() {
        observateurs.forEach((observateur -> {
            observateur.actualiser(vue.getEcranSuivant());
        }));
    }

    @Override
    public void initialisation() throws IOException {
        FXMLLoader loader = new FXMLLoader(RegateApplication.class.getResource("menu_principal.fxml"), RegateApplication.getMessageBundle());
        root = loader.load();
        vue = loader.getController();
        vue.setControleur(this);
    }

    public void onLblClick() {
        notifier();
    }

    public Parent getRoot() {
        return root;
    }
}

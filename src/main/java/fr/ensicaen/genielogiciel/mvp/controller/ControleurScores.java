package fr.ensicaen.genielogiciel.mvp.controller;

import fr.ensicaen.genielogiciel.mvp.RegateApplication;
import fr.ensicaen.genielogiciel.mvp.model.score.Ranking;
import fr.ensicaen.genielogiciel.mvp.view.VueScore;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

import java.io.IOException;

public class ControleurScores extends Controleur {
    private VueScore vue;
    private Parent root;
    private Ranking rk;

    public ControleurScores() {
        super();
    }

    @Override
    public void notifier() {
        // TODO: (A appeler pour changer l'état de l'application des scores au menu principal ou au jeu)
    }

    @Override
    public void initialisation() throws IOException {
        FXMLLoader loader = new FXMLLoader(RegateApplication.class.getResource("scores.fxml"), RegateApplication.getMessageBundle());
        root = loader.load();
        vue = loader.getController();
        vue.setControleur(this);
    }

    @Override
    public Parent getRoot() {
        return root;
    }

    public String loadScoreFile(){
        rk = new Ranking();
        rk.load();
        String scoreText = "";
        for (int i =0;i<rk.getIndexLast();i++){
            scoreText += " "+(i+1)+". "+rk.getScores()[i].name+" : "+rk.getScores()[i].score+"\n";
        }
        return scoreText;
    }
}

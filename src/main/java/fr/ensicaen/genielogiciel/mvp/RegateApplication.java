package fr.ensicaen.genielogiciel.mvp;

import java.io.IOException;
import java.util.ResourceBundle;

import fr.ensicaen.genielogiciel.mvp.controller.*;
import fr.ensicaen.genielogiciel.mvp.controller.ControleurJeu;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public final class RegateApplication extends Application implements Observateur {
    public static void main(String[] args) {
        launch(args);
    }

    private static Scene currentScene;
    private static Stage stage;
    private Controleur currentControleur;

    private static int WIDTH = 1200;
    private static int HEIGHT = 720;

    public static ResourceBundle getMessageBundle() {
        return ResourceBundle.getBundle("fr.ensicaen.genielogiciel.mvp.MessageBundle");
    }

    @Override
    public void start(final Stage primaryStage) throws Exception {
        stage = primaryStage;

        stage.setTitle("Menu principal");
        currentControleur = new ControleurMenuPrincipal();
        currentControleur.ajouterObservateur(this);
        currentControleur.initialisation();
        Parent root = currentControleur.getRoot();

        currentScene = new Scene(root, WIDTH, HEIGHT);
        stage.setScene(currentScene);
        stage.show();
    }

    @Override
    public void actualiser(Ecrans nouveauControleur) {
        switch (nouveauControleur) {
            case JEU -> {
                currentControleur = new ControleurJeu();
                stage.setTitle("Jeu");
            }
            case SCORE -> {
                currentControleur = new ControleurScores();
                stage.setTitle("Scores");
            }
            case MENU_PRINCIPAL -> {
                currentControleur = new ControleurMenuPrincipal();
                stage.setTitle("Menu principal");
            }
        }

        currentControleur.ajouterObservateur(this);
        try {
            currentControleur.initialisation();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Parent root = currentControleur.getRoot();
        if (root != null) {
            currentScene = new Scene(root, WIDTH, HEIGHT);
            stage.setScene(currentScene);
        }
    }

    public static Stage getScene() {
        return stage;
    }
}

package fr.ensicaen.genielogiciel.mvp.model.jeu;

import fr.ensicaen.genielogiciel.mvp.RegateApplication;

import java.io.IOException;
import java.util.ArrayList;

public class GameManager {
  private Carte carte;
  private ArrayList<Voilier> voiliers;

  public GameManager() {
    carte = new Carte();
    voiliers = new ArrayList<>();
  }

  public void initialisation() throws IOException {
    carte.setStrategieDeChangementCarte(new ChargerCarteDepuisFichier(RegateApplication.class.getResource("map.save")));
    carte.chargerCarte();

    Voilier voilier = new VoilierConcret("mon voilier",
            new Vector2D(500, 320),
            new ChargerPolaireDeVitesseDepuisFichier(RegateApplication.class.getResource("vitesse_angle.pol")));
    voilier.chargerPolaire();
    voiliers.add(voilier);

    carte.genererVent(voilier.getPolaireDeVitesse());
    voilier.initDirectionVent(carte.getVent().getDirection());
  }

  public boolean verifierCollisions() {
    return carte.enCollision(voiliers.get(0).getForme());
  }

  public boolean verifierLigneArrivee() {
    return carte.passeLaLigneArrivee(voiliers.get(0).getForme());
  }

  public Carte getCarte() {
    return carte;
  }
  public Voilier getVoilier() {return voiliers.get(0);}
}

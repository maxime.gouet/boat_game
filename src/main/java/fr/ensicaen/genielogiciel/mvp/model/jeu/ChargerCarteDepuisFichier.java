package fr.ensicaen.genielogiciel.mvp.model.jeu;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class ChargerCarteDepuisFichier implements IChargerCarte {
    private BufferedReader Breader;

    public ChargerCarteDepuisFichier(URL filePath) {
        String FilePath = filePath.getPath();
        try {
            FileReader reader = new FileReader(FilePath);
            Breader = new BufferedReader(reader);
            Breader.mark(1000);
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    @Override
    public ArrayList<Bouee> chargerBoueee() throws IOException{
        Breader.reset();
        ArrayList<Bouee> list = new ArrayList<Bouee>();
        String line;
        while ((line = Breader.readLine()) != null) {
            String[] Array = line.split(",");
            if(Array[0].equals("C")){
                Bouee B = new Bouee(new Point(Integer.parseInt(Array[1].split(":")[0]),Integer.parseInt(Array[1].split(":")[1])));
                list.add(B);
            }
        }
        return list;
    }

    @Override
    public Bordure chargerBordure() throws IOException{
        Breader.reset();
        String line;
        while ((line = Breader.readLine()) != null) {
            String[] Array = line.split(",");
            if(Array[0].equals("B")){
                ArrayList<Point> list = new ArrayList<Point>();
                for(int i=1;i<Array.length;i++){
                    list.add(new Point(Integer.parseInt(Array[i].split(":")[0]),Integer.parseInt(Array[i].split(":")[1])));
                }
                return new Bordure(list);
            }
        }
        return null;
    }

    @Override
    public Vent chargerVent() throws IOException{
        Breader.reset();
        String line;
        while ((line = Breader.readLine()) != null) {
            String[] Array = line.split(",");
            if(Array[0].equals("V")){
                Direction dir = switch (Array[2]) {
                    case "NORD" -> Direction.NORD;
                    case "SUD" -> Direction.SUD;
                    case "OUEST" -> Direction.OUEST;
                    case "EST" -> Direction.EST;
                    case "NORDEST" -> Direction.NORDEST;
                    case "NORDOUEST" -> Direction.NORDOUEST;
                    case "SUDEST" -> Direction.SUDEST;
                    case "SUDOUEST" -> Direction.SUDOUEST;
                    default -> Direction.NORD;
                };
                return new Vent(Float.parseFloat(Array[1]),dir);
            }
        }
        return null;
    }

    @Override
    public LigneArrive chargerLigneArrive() throws IOException {
        Breader.reset();
        String line;
        while ((line = Breader.readLine()) != null) {
            String[] Array = line.split(",");
            if(Array[0].equals("L")){
                ArrayList<Point> list = new ArrayList<Point>();
                for(int i=1;i<Array.length;i++){
                    list.add(new Point(Integer.parseInt(Array[i].split(":")[0]),Integer.parseInt(Array[i].split(":")[1])));
                }
                return new LigneArrive(list);
            }
        }
        return null;
    }
}

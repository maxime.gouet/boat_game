package fr.ensicaen.genielogiciel.mvp.model.score;

import com.google.gson.Gson;
import fr.ensicaen.genielogiciel.mvp.RegateApplication;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;

public class Ranking {
    private int indexLast;
    private ScorePlayer[] scores;

    public int getIndexLast() {
        return indexLast;
    }

    public ScorePlayer[] getScores() {
        return scores;
    }

    public Ranking() {
        scores = new ScorePlayer[100];
        indexLast = 0;

        for(int i = 0; i < 100; i++) {
            scores[i] = new ScorePlayer();
        }
    }

    public Ranking(String rankingString) {
        Ranking temp = new Gson().fromJson(rankingString, Ranking.class);

        this.indexLast = temp.indexLast;
        this.scores = temp.scores;
    }

    public void add(String name, long score) {
        if(indexLast < 99 || (indexLast == 99 && scores[indexLast].score < score)) {
            scores[indexLast] = new ScorePlayer(name, score);
        }

        if(indexLast < 99) {
            indexLast++;
        }

        Arrays.sort(scores);
    }

    public String toString() {
        return new Gson().toJson(this);
    }

    public String toStringTable() {
        ArrayList<String> scoresStringArray = new ArrayList<>();

        int i = 0;
        while(i < indexLast) {
            scoresStringArray.add(scores[i].name + " : " + scores[i].score);
            i++;
        }

        return new Gson().toJson(scoresStringArray).replace("\"", "\\\"");
    }

    public void load() {
        File file = new File(RegateApplication.class.getResource("ranking.json").getPath());
        if (file != null) {
            try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
                String line = reader.readLine();
                Ranking temp = new Ranking(line);

                this.indexLast = temp.indexLast;
                this.scores = temp.scores;
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }
    }

    public void save() {
        File file = new File(RegateApplication.class.getResource("ranking.json").getPath());
        if (file != null) {
            try {
                PrintWriter writer;
                writer = new PrintWriter(file);
                writer.println(this.toString());
                writer.close();
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}


package fr.ensicaen.genielogiciel.mvp.model.jeu;

public class DonneesPolaire {
    private int[] noeuds;
    private float[][] vitesses;

    public DonneesPolaire(int[] noeuds, float[][] vitesses) {
        this.noeuds = noeuds;
        this.vitesses = vitesses;
    }

    public int[] getNoeuds() {
        return noeuds;
    }

    public void setNoeuds(int[] noeuds) {
        this.noeuds = noeuds;
    }

    public float[][] getVitesses() {
        return vitesses;
    }

    public void setVitesses(float[][] vitesses) {
        this.vitesses = vitesses;
    }
}
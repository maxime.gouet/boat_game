package fr.ensicaen.genielogiciel.mvp.model.jeu;

import java.io.IOException;
import java.util.ArrayList;

public interface IChargerCarte {

    public ArrayList<Bouee> chargerBoueee() throws IOException;

    public Bordure chargerBordure() throws IOException;

    public Vent chargerVent() throws IOException;

    public LigneArrive chargerLigneArrive() throws IOException;
}

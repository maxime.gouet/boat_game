package fr.ensicaen.genielogiciel.mvp.model.jeu;

import fr.ensicaen.genielogiciel.mvp.controller.IChargerPolaireDeVitesse;

public class VoilierConcret extends Voilier {
    private String nom;

    public VoilierConcret(String nom,
                          Vector2D position,
                          IChargerPolaireDeVitesse strategie) {
        super(position, strategie);
        this.nom = nom;
    }

    public String getNom() {
        return nom;
    }
}

package fr.ensicaen.genielogiciel.mvp.model.score;

import java.util.Comparator;

public class ScorePlayer implements Comparable<ScorePlayer>, Comparator<ScorePlayer> {
    public String name;
    public long score;

    public ScorePlayer() {
        name = "";
        score = 0;
    }

    public ScorePlayer(String name, long score) {
        this.name = name;
        this.score = score;
    }

    @Override
    public int compareTo(ScorePlayer o) {
        return -Long.compare(this.score, o.score);
    }

    @Override
    public int compare(ScorePlayer o1, ScorePlayer o2) {
        return -Long.compare(o1.score, o2.score);
    }
}

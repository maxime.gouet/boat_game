package fr.ensicaen.genielogiciel.mvp.model.jeu;

public enum Direction {
    NORD,
    NORDEST,
    NORDOUEST,
    SUD,
    SUDEST,
    SUDOUEST,
    OUEST,
    EST
}

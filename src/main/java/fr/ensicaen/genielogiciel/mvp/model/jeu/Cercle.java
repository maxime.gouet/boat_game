package fr.ensicaen.genielogiciel.mvp.model.jeu;

import java.util.ArrayList;

public class Cercle extends Forme {
    private int rayonCercle;

    public Cercle(int rayonCercle, Point p) {
        this.rayonCercle = rayonCercle;
        setListePoints(new ArrayList<Point>());
        listePoints.add(p);
    }

    public Point getPosition() {
        return listePoints.get(0);
    }


    // GETTERS
    final public int getRayonCercle() {
        return this.rayonCercle;
    }

    // SETTERS
    final public void setRayonCercle(int r) {
        this.rayonCercle = r;
    }

    @Override
    public boolean enCollision(Forme p) {
        for (Point pts: p.getListePoints()) {
            if(Math.sqrt(Math.pow(pts.getX()-this.listePoints.get(0).getX(),2)+Math.pow(pts.getY()-this.listePoints.get(0).getY(),2)) < this.rayonCercle){
                return true;
            }
        }
        return false;
    }
}



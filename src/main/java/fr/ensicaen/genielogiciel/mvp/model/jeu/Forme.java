package fr.ensicaen.genielogiciel.mvp.model.jeu;

import java.util.ArrayList;

public abstract class Forme  {
    protected ArrayList<Point> listePoints;
    
    public Forme() {
        super();
        listePoints= new ArrayList<Point>();
    }

    public abstract boolean enCollision(Forme f);

    public ArrayList<Point> getListePoints() {
        return listePoints;
    }

    public void setListePoints(ArrayList<Point> listePoints) {
        this.listePoints = listePoints;
    }
}

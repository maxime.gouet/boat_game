package fr.ensicaen.genielogiciel.mvp.model.jeu;

import java.util.ArrayList;

public class Bordure {
    private Polygone polygone;

    public Bordure(ArrayList<Point> point) {
        polygone = new Polygone(point);
    }

    public boolean enCollision(Forme formeVoilier) {
        return polygone.enCollision(formeVoilier);
    }

    public Polygone getPolygone() {
        return polygone;
    }

    public void setPolygone(Polygone polygone) {
        this.polygone = polygone;
    }
}

package fr.ensicaen.genielogiciel.mvp.model.jeu;

import java.util.ArrayList;

public class LigneArrive {
    private Polygone polygone;

    public LigneArrive(ArrayList<Point> point) {
        polygone = new Polygone(point);
    }

    public boolean isArriver(Forme formeVoilier) {
        return polygone.enCollision(formeVoilier);
    }

    public Polygone getPolygone() {
        return polygone;
    }
}

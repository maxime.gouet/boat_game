package fr.ensicaen.genielogiciel.mvp.model.jeu;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import fr.ensicaen.genielogiciel.mvp.controller.IChargerPolaireDeVitesse;

public class Carte {
    private ArrayList<Bouee> bouees;
    private Bordure bordure;
    private Vent vent;
    private IChargerCarte strategieChargementCarte;
    private LigneArrive ligneArrive;

    public Carte() {
        bouees = new ArrayList<Bouee>();
        bordure = null;
        vent = null;
        strategieChargementCarte = null;
        ligneArrive = null;
    }

    public boolean passeLaLigneArrivee(Forme formeVoilier) {
        boolean toReturn = true;
        for (Bouee bouee : bouees) {
            if (!bouee.getCheckpoint()) {
                toReturn = false;
                break;
            }
        }
        if (toReturn)
            return ligneArrive.isArriver(formeVoilier);
        return false;
    }

    public void genererVent(PolaireDeVitesse polaireDeVitesse) {
        DonneesPolaire donnees = polaireDeVitesse.getDonnees();

        vent = new Vent(randomVitesse(donnees.getNoeuds()),randomDirection());
    }

    private Direction randomDirection() {
        int index = new Random().nextInt(Direction.values().length);
        return Direction.values()[index];
    }

    private int randomVitesse(int[] noeuds) {
        int randomIndexVitesse = new Random().nextInt(noeuds.length);
        return noeuds[randomIndexVitesse];
    }

    public boolean enCollision(Forme formeVoilier) {
        boolean toReturn = false;

        for (Bouee bouee : bouees){
            if (bouee.enCollision(formeVoilier))
                toReturn = true;
        }

        if (bordure.enCollision(formeVoilier))
            toReturn = true;

        return toReturn;
    }

    public void setStrategieDeChangementCarte(IChargerCarte strategie)
    {
        this.strategieChargementCarte = strategie;
    }

    public Bordure getBordure() {
        return bordure;
    }

    public ArrayList<Bouee> getBouees() {
        return bouees;
    }

    public void chargerCarte() throws IOException {
        bordure = strategieChargementCarte.chargerBordure();
        bouees = strategieChargementCarte.chargerBoueee();
        vent = strategieChargementCarte.chargerVent();
        ligneArrive = strategieChargementCarte.chargerLigneArrive();
    }

    public LigneArrive getLigneArrive() {
        return ligneArrive;
    }

    public Vent getVent() {
        return vent;
    }
}

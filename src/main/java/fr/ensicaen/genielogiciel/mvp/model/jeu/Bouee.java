package fr.ensicaen.genielogiciel.mvp.model.jeu;

public class Bouee {
    private Cercle cercle;
    private Cercle cercle_checkpoint;
    private boolean checkpoint;

    public Bouee(Point point) {
        cercle = new Cercle(10, point);
        cercle_checkpoint = new Cercle(30, point);
        checkpoint = false;
    }

    public boolean enCollision(Forme forme) {
        if (cercle_checkpoint.enCollision(forme))
            checkpoint = true;
        return cercle.enCollision(forme);
    }

    public Point getPosition()
    {
        return cercle.getPosition();
    }

    public int getRayon()
    {
        return cercle.getRayonCercle();
    }

    public boolean getCheckpoint() {
        return checkpoint;
    }
}

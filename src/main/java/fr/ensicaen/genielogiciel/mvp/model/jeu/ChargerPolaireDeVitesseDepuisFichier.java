package fr.ensicaen.genielogiciel.mvp.model.jeu;

import fr.ensicaen.genielogiciel.mvp.RegateApplication;
import fr.ensicaen.genielogiciel.mvp.controller.IChargerPolaireDeVitesse;
import fr.ensicaen.genielogiciel.mvp.model.jeu.DonneesPolaire;
import fr.ensicaen.genielogiciel.mvp.model.jeu.PolaireDeVitesse;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class ChargerPolaireDeVitesseDepuisFichier implements IChargerPolaireDeVitesse {
    private String path;

    public ChargerPolaireDeVitesseDepuisFichier(URL _path) {
        path = _path.getPath();
    }

    @Override
    public PolaireDeVitesse charger() throws IOException {
        String vitesseVent = null;
        List<String> allAngleVentEtValeur = new ArrayList<String>();

        FileReader reader = new FileReader(path);
        BufferedReader br = new BufferedReader(reader);

        // read line by line
        String line;
        int i = 0;
        while ((line = br.readLine()) != null) {
            if (i == 0) {
                vitesseVent = line;
            } else {
                allAngleVentEtValeur.add(line);
            }

            i++;
        }

        int[] noeuds = new int[vitesseVent.split("\\t").length -1];
        int k = 0;
        for (String elt : vitesseVent.split("\\t")) {
            if(k != 0)
                noeuds[k-1] = Integer.valueOf(elt);
            k++;
        }

        float[][] vitesses = new float[vitesseVent.split("\\t").length -1][allAngleVentEtValeur.size() ];
        i = 0;
        int j = 0;
        for (String ligne :  allAngleVentEtValeur) {
            for (String elt : ligne.split("\\t")) {
                if(i != 0) {
                    vitesses[i-1][j] = Float.valueOf(elt);
                }
                i++;
            }
            i = 0;
            j++;
        }

        return new PolaireDeVitesse(new DonneesPolaire(noeuds, vitesses));
    }
}

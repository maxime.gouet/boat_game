package fr.ensicaen.genielogiciel.mvp.model.jeu;

import fr.ensicaen.genielogiciel.mvp.controller.IChargerPolaireDeVitesse;

import java.io.IOException;
import java.util.ArrayList;

public abstract class Voilier {
    private int directionParRaportAuVent;
    private double vitesse;
    private Vector2D position;
    private Vector2D direction;
    private PolaireDeVitesse polaireDeVitesse;
    private IChargerPolaireDeVitesse strategie;

    private ArrayList<ObservateurVoilier> observateurs;

    private final int WIDTH_BATEAU = 10;
    private final int HEIGHT_BATEAU = 5;
    private final double DIVIDE_SPEED = 5.0; // Je l'utilise afin de régler la vitesse "graphique"

    public Voilier(Vector2D _position, IChargerPolaireDeVitesse _strategie) {
        vitesse = 1.5f;
        directionParRaportAuVent = 90;
        position = _position;
        strategie = _strategie;
        direction = new Vector2D(1,0);
        observateurs = new ArrayList<>();
    }

    public void deplacer(Vent vent) {
        Vector2D velocite = new Vector2D(0, 0);
        direction.normalize();
        velocite.add(direction);
        vitesse = polaireDeVitesse.getVitesse(Math.abs(directionParRaportAuVent), (int)vent.getVitesse());
        velocite.multiply(vitesse);
        velocite.divide(DIVIDE_SPEED);

        observateurs.forEach((ob) -> {
            ob.vitesseUpdated(vitesse);
        });

        position.add(velocite);
    }

    public void initDirectionVent(Direction directionVent) {
        switch (directionVent) {
            case EST -> {
                directionParRaportAuVent = -90;
            }
            case NORDEST -> {
                directionParRaportAuVent = -45;
            }
            case NORD -> {
                directionParRaportAuVent = 0;
            }
            case NORDOUEST -> {
                directionParRaportAuVent = 45;
            }
            case OUEST -> {
                directionParRaportAuVent = 90;
            }
            case SUDOUEST -> {
                directionParRaportAuVent = 135;
            }
            case SUD -> {
                directionParRaportAuVent = 180;
            }
            case SUDEST -> {
                directionParRaportAuVent = -135;
            }
        }
    }

    public Vector2D getPosition() {
        return position;
    }

    public PolaireDeVitesse getPolaireDeVitesse() {
        return polaireDeVitesse;
    }

    public void chargerPolaire() throws IOException {
        polaireDeVitesse = strategie.charger();
    }

    public int getRotation() {
        return (int) Math.toDegrees(direction.getAngle());
    }

    public double getVitesse() {
        return vitesse;
    }

    public void addObservateur(ObservateurVoilier ob) {
        observateurs.add(ob);
    }

    public void updateOrientation(double step) {
        direction.rotateBy(step);
        directionParRaportAuVent += Math.toDegrees(step);
        if (directionParRaportAuVent > 180) directionParRaportAuVent = -180;
        if (directionParRaportAuVent < -180) directionParRaportAuVent = 180;

        observateurs.forEach((ob) -> {
            ob.directionParRapportAuVentUpdated(directionParRaportAuVent);
        });
    }

    public Forme getForme() {
        ArrayList<Point> points = new ArrayList<>();

        points.add(new Point((int)position.x-WIDTH_BATEAU/2, (int)position.y-HEIGHT_BATEAU/2)); // HAUT GAUCHE
        points.add(new Point((int)position.x+WIDTH_BATEAU/2, (int)position.y-HEIGHT_BATEAU/2)); // HAUT DROIT
        points.add(new Point((int)position.x+WIDTH_BATEAU/2, (int)position.y+HEIGHT_BATEAU/2)); // BAS DROIT
        points.add(new Point((int)position.x-WIDTH_BATEAU/2, (int)position.y+HEIGHT_BATEAU/2)); // BAS GAUCHE

        return new Polygone(points);
    }
}

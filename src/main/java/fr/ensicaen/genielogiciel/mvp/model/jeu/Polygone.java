package fr.ensicaen.genielogiciel.mvp.model.jeu;

import java.util.ArrayList;

public class Polygone extends Forme {

    public Polygone(ArrayList<Point> points) {
        super();
        setListePoints(points);
    }

    public boolean calculCollision(Point A, Point B, Point C) {
        return (C.getY()-A.getY()) * (B.getX()-A.getX()) > (B.getY()-A.getY()) * (C.getX()-A.getX());
    }

    @Override
    public boolean enCollision(Forme p) {
        for (int i = 0 ; i < this.listePoints.size() ; i++) {
            for (int j = 0 ; j < p.listePoints.size(); j++) {
                int jIndex = j;
                int iIndex = i;
                
                if (i == 0) iIndex = this.listePoints.size()-1;
                else iIndex = i-1;
                if (j == 0) jIndex = p.listePoints.size()-1;
                else jIndex = j-1;
                
                if(calculCollision(this.listePoints.get(i), p.listePoints.get(j), p.listePoints.get(jIndex)) != calculCollision(this.listePoints.get(iIndex), p.listePoints.get(j), p.listePoints.get(jIndex)) && calculCollision(this.listePoints.get(i), this.listePoints.get(iIndex), p.listePoints.get(j)) != calculCollision(this.listePoints.get(i), this.listePoints.get(iIndex), p.listePoints.get(jIndex))){
                    return calculCollision(this.listePoints.get(i), p.listePoints.get(j), p.listePoints.get(jIndex)) !=
                            calculCollision(this.listePoints.get(iIndex), p.listePoints.get(j), p.listePoints.get(jIndex)) &&
                            calculCollision(this.listePoints.get(i), this.listePoints.get(iIndex), p.listePoints.get(j)) != calculCollision(this.listePoints.get(i), this.listePoints.get(iIndex), p.listePoints.get(jIndex));
                }
            }
        }
        return false;
    }
}

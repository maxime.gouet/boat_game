package fr.ensicaen.genielogiciel.mvp.model.jeu;

public class Vent {
    private float vitesse;
    private Direction directionVent;

    public Vent (float vitesse, Direction direction) {
        this.vitesse = vitesse ;
        this.directionVent = direction;
    }

    public float getVitesse() {
        return vitesse ;
    }

    public Direction getDirection(){
        return directionVent;
    }

    public void setDirection(Direction direction) {
        this.directionVent = direction;
    }
    public void setVitesse(float vitesse) {
        this.vitesse = vitesse ;
    }

    public String toString() {
        return "[VENT] "+ this.vitesse + " - "+this.directionVent;
    }
}


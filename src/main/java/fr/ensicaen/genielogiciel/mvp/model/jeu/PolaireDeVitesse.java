package fr.ensicaen.genielogiciel.mvp.model.jeu;

public class PolaireDeVitesse {
    private DonneesPolaire donnees;

    public PolaireDeVitesse(DonneesPolaire donnees) {
        this.donnees = donnees;
    }

    public float getVitesse(int orientationVoilier, int vitesseVent) {
        int length = donnees.getNoeuds().length;
        orientationVoilier = Math.abs(orientationVoilier);
        if (orientationVoilier == 0)
            orientationVoilier = 1;

        int i = 0;
        while (i < length) {
            if (vitesseVent == donnees.getNoeuds()[i]) {
                break;
            }

            i++;

            if (i == length) {
                return (float) 1.0;
            }
        }

        return donnees.getVitesses()[i][orientationVoilier - 1];
    }

    public DonneesPolaire getDonnees() {
        return donnees;
    }
}

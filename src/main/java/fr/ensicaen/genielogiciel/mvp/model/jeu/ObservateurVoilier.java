package fr.ensicaen.genielogiciel.mvp.model.jeu;

public interface ObservateurVoilier {
    public void vitesseUpdated(double vitesse);
    public void directionParRapportAuVentUpdated(int direction);
}

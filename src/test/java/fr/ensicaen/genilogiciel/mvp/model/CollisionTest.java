package fr.ensicaen.genilogiciel.mvp.model;

import fr.ensicaen.genielogiciel.mvp.model.jeu.Cercle;
import fr.ensicaen.genielogiciel.mvp.model.jeu.Point;
import fr.ensicaen.genielogiciel.mvp.model.jeu.Polygone;
import org.junit.Test;
import java.util.ArrayList;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class CollisionTest {
    @Test
    public void testCollisionCercleFalse(){
        Cercle cer = new Cercle(1,new Point(2,2));
        ArrayList<Point> ar = new ArrayList<Point>();
        ar.add(new Point(4,4));
        ar.add(new Point(5,5));
        ar.add(new Point(4,5));
        Polygone poly = new Polygone(ar);
        assertFalse("Collision doit etre fausse",cer.enCollision(poly));
    }
    @Test
    public void testCollisionCercleTrue(){
        Cercle cer = new Cercle(1,new Point(2,2));
        ArrayList<Point> ar = new ArrayList<Point>();
        ar.add(new Point(2,2));
        ar.add(new Point(4,4));
        ar.add(new Point(2,4));
        Polygone poly = new Polygone(ar);
        assertTrue("Collision doit etre Vrai",cer.enCollision(poly));
    }
    @Test
    public void testCollisionPolygoneFalse(){
        ArrayList<Point> ab = new ArrayList<Point>();
        ab.add(new Point(0,0));
        ab.add(new Point(0,10));
        ab.add(new Point(10,10));
        ab.add(new Point(10,0));
        Polygone map = new Polygone(ab);
        ArrayList<Point> ar = new ArrayList<Point>();
        ar.add(new Point(4,4));
        ar.add(new Point(5,5));
        ar.add(new Point(4,5));
        Polygone boat = new Polygone(ar);
        assertFalse("Collision doit etre fausse",map.enCollision(boat));
    }
    @Test
    public void testCollisionPolygoneTrue(){
        ArrayList<Point> ab = new ArrayList<Point>();
        ab.add(new Point(0,0));
        ab.add(new Point(0,10));
        ab.add(new Point(10,10));
        ab.add(new Point(10,0));
        Polygone map = new Polygone(ab);
        ArrayList<Point> ar = new ArrayList<Point>();
        ar.add(new Point(9,9));
        ar.add(new Point(11,11));
        ar.add(new Point(9,11));
        Polygone boat = new Polygone(ar);
        assertTrue("Collision doit etre vrai",map.enCollision(boat));
    }
}

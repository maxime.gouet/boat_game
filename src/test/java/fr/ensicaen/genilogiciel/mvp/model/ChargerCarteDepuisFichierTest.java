package fr.ensicaen.genilogiciel.mvp.model;

import fr.ensicaen.genielogiciel.mvp.model.jeu.*;
import javafx.beans.binding.When;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class ChargerCarteDepuisFichierTest {
    @Test
    public void test_chargerBoueee() throws IOException {

        final BufferedReader bReader = Mockito.mock(
                BufferedReader.class);

        final ChargerCarteDepuisFichier _ChargerCarteDepuisFichier =
                Mockito.mock(
                        ChargerCarteDepuisFichier.class,
                        Mockito.CALLS_REAL_METHODS);

        try {
            when(bReader.readLine()).thenReturn("C,50:50").thenReturn("C,150:100").thenReturn("C,500:300").thenReturn("B,5:5,5:715,995:715,995:5").thenReturn(null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ReflectionTestUtils.setField(_ChargerCarteDepuisFichier, "Breader", bReader);

        ArrayList<Bouee> boueeArrayList = _ChargerCarteDepuisFichier.chargerBoueee();
        assertTrue("test charger bouee",boueeArrayList.get(1).getPosition().getX()==150 && boueeArrayList.get(1).getPosition().getY()==100);
        assertEquals("test charge bouee", boueeArrayList.size() , 3);

    }
    @Test
    public void test_chargerVent() throws IOException {

        final BufferedReader bReader = Mockito.mock(
                BufferedReader.class);

        final ChargerCarteDepuisFichier _ChargerCarteDepuisFichier =
                Mockito.mock(
                        ChargerCarteDepuisFichier.class,
                        Mockito.CALLS_REAL_METHODS);

        try {
            when(bReader.readLine()).thenReturn("B,5:5,5:715,995:715,995:5").thenReturn("V,6,SUD").thenReturn("L,0:0,0:1,1:1,1:0").thenReturn(null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ReflectionTestUtils.setField(_ChargerCarteDepuisFichier, "Breader", bReader);

        Vent _vent = _ChargerCarteDepuisFichier.chargerVent();
        assertTrue("test charger Vent",_vent.getVitesse() == 6 && _vent.getDirection() == Direction.SUD);

    }
    @Test
    public void test_chargerLigneArrive() throws IOException {

        final BufferedReader bReader = Mockito.mock(
                BufferedReader.class);

        final ChargerCarteDepuisFichier _ChargerCarteDepuisFichier =
                Mockito.mock(
                        ChargerCarteDepuisFichier.class,
                        Mockito.CALLS_REAL_METHODS);

        try {
            when(bReader.readLine()).thenReturn("B,5:5,5:715,995:715,995:5").thenReturn("V,6,SUD").thenReturn("L,0:0,0:15,1:1,9:0").thenReturn(null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ReflectionTestUtils.setField(_ChargerCarteDepuisFichier, "Breader", bReader);

        LigneArrive _ligne= _ChargerCarteDepuisFichier.chargerLigneArrive();
        assertTrue("test charger Vent","[{0:0}, {0:15}, {1:1}, {9:0}]".equals(_ligne.getPolygone().getListePoints().toString()));
    }
    @Test
    public void test_chargerBordure() throws IOException {

        final BufferedReader bReader = Mockito.mock(
                BufferedReader.class);

        final ChargerCarteDepuisFichier _ChargerCarteDepuisFichier =
                Mockito.mock(
                        ChargerCarteDepuisFichier.class,
                        Mockito.CALLS_REAL_METHODS);

        try {
            when(bReader.readLine()).thenReturn("B,5:5,5:715,995:715,995:5").thenReturn("V,6,SUD").thenReturn("L,0:0,0:15,1:1,9:0").thenReturn(null);
        } catch (IOException e) {
            e.printStackTrace();
        }
        ReflectionTestUtils.setField(_ChargerCarteDepuisFichier, "Breader", bReader);

        Bordure _bordure= _ChargerCarteDepuisFichier.chargerBordure();
        assertTrue("test charger Bordure","[{5:5}, {5:715}, {995:715}, {995:5}]".equals(_bordure.getPolygone().getListePoints().toString()));
    }
}

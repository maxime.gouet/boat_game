package fr.ensicaen.genilogiciel.mvp.model;

import fr.ensicaen.genielogiciel.mvp.model.jeu.Forme;
import fr.ensicaen.genielogiciel.mvp.model.jeu.Point;
import fr.ensicaen.genielogiciel.mvp.model.jeu.Voilier;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;


import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;


public class FormeTest {


    @Test
    public void testSetterGetter_setList() {

        final Forme forme = Mockito.mock(
                Forme.class,
                Mockito.CALLS_REAL_METHODS);

        Point point = Mockito.mock(Point.class);
        when(point.getX()).thenReturn(15);
        when(point.getY()).thenReturn(25);
        ArrayList<Point> testListePoints = new ArrayList<>(Arrays.asList(point));

        forme.setListePoints(testListePoints);

        ArrayList<Point> getList = forme.getListePoints();
        assertTrue("test set list", getList.get(0).getX()==point.getX() && getList.get(0).getY()==point.getY());

    }





}

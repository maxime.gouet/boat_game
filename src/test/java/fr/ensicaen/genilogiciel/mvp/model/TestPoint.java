package fr.ensicaen.genilogiciel.mvp.model;

import fr.ensicaen.genielogiciel.mvp.model.jeu.Point;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class TestPoint {

    private static Point mockPoint;
    private static Point point1; // Donner coordonnées ici pour point1 ?

    @BeforeClass
    public static void init() {

        mockPoint = mock(Point.class);
        //when(mockPoint.getX().thenReturn(point1.getX());
        //when(mockPoint.getY().thenReturn(point1.getY());
    }

    @Test
    public void getXTest() {
        int x = 10;
        x = mockPoint.getX();
        assertNotNull(x);
        //assertEquals(int.valueOf("10"), point1.getX()); // value of ou size ??
    }

    @Test
    public void getYTest() {
        int y = 15; // init mockpoint Y ?
        y = mockPoint.getY();
        assertNotNull(y);
        //assertEquals(int.valueOf("15"), point1.getY()); // value of ou size ??
    }

    @Test
    public void buildPointTest() { // A corriger
        //point1 = mockPoint(10,15); // new a add ?
        //assertNotNull(point1);
        //assertEquals();
    }

    @Test
    public void multiplecallPointTest() {

        //Point point2 = mockPoint(x,y)
        //x = mockEmployeeDAO.getX();
        //y = mockEmployeeDAO.getY();
        //System.out.println(point2);

        //x = mockEmployeeDAO.getX();
        //y = mockEmployeeDAO.getY();
        //System.out.println(point2);

        //x = mockEmployeeDAO.getX();
        //y = mockEmployeeDAO.getY();
        //System.out.println(point2);
    }
}

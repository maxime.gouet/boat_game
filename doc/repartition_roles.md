# Répartition des rôles  


## Chef de projet

> C'est la personne chargée de mener un projet et de gérer son bon déroulement.

1. Lucas GUILBERT
2. Elyan DESMAREST

## Architecte

> C'est la personne responsable de la création et du respect du modèle d'architecture logicielle.

1. Maxime GOUET
2. Lucas GUILBERT

## Développeur

> C'est la personne qui réalise des logiciels et les met en œuvre à l'aide de langages de programmation. 

- Lucas GUILBERT
- Nicolas HODIESNE
- Théo BERARD
- Gabin PIERRE
- Stuart TATHAM
- Elyan DESMAREST
- Maxime GOUET
- Alexis MAIRENA

## Responsable de version

> C'est la personne responsable du dépôt sur Gitlab. C'est lui qui fait l'intégration continue du travail
des développeurs et il est garant de la propreté du code et des tests.


1. Stuart TATHAM
2. Alexis MAIRENA
